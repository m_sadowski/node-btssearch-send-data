Aplikacja do hurtowej wysyłki danych o BTS-ach sieci komórkowych do serwisu btssearch.pl (POST do http://btsearch.pl/dodaj.php).

**Instalacja**

Zainstaluj biblioteki poleceniem "npm install"

**Wysyłanie danych**

W folderze "xls_sample" jest wzorzec pliku excela, który należy uzupełnić danymi o stacjach BTS sieci komórkowych i wyeksportować go w formacie "csv" do pliku "./input/btsy.csv".

Uruchomienie programu:
"npm run start nr_linii_początkowej nr_linii_końcowej"

np:
"npm run start 12 13" - wyśle linie od 12 do 13 z pliku csv.

Wprowadzone jest zabezpieczenie - wysłanie danych musi być zatwierdzone dowolnym klawiszem. Ctrl+C - anulowanie wysyłki.