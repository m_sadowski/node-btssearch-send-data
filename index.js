const fs = require('fs');
const axios = require('axios');
const qs = require('querystring');
const moment = require('moment');
const pressAnyKey = require('press-any-key');
const csv = require('csv');

const processArgs = process.argv.slice(2);
if (processArgs.length < 2) {
  console.log('Please provide start and end line in args !!!');
  process.exit(1);
}

const csvInputFile = './input/btsy.csv';
const csvInputLineStart = Number(processArgs[0]);
const csvInputLineEnd = Number(processArgs[1]);

const logsDir = './logs';
// const btsSearchUrl = 'https://n5hfabocll.execute-api.eu-central-1.amazonaws.com/dev/btssearch';  // FAKE server
const btsSearchUrl = 'http://btsearch.pl/dodaj.php';

const readCsvInputData = () => {
  return new Promise((resolve, reject) => {
    
    const csvObj = csv();
    const btsData = [];
    try {
      csvObj.from.path(csvInputFile).to.array(function (data) {
        for (var index = csvInputLineStart - 1; index < csvInputLineEnd; index++) {
          btsData.push({
            siec: data[index][5],
            pasmo: data[index][6],
            lac: data[index][11],
            sektor_1: data[index][8],
            sektor_2: data[index][9],
            sektor_3: data[index][10],
            wojewodztwo: data[index][1],
            miejscowosc: data[index][2],
            lokalizacja: data[index][3],
            eNodeB: data[index][7]
          });
        }
        resolve(btsData);
      });
    }
    catch (err) {
      reject(err);
    }
  });
};

const sendDataToBtsSearch = async({
  siec,
  pasmo,
  lac,
  sektor_1,
  sektor_2,
  sektor_3,
  wojewodztwo,
  miejscowosc,
  lokalizacja,
  eNodeB
}) => {

  return new Promise(async (resolve, reject) => {
    const requestBody = {
      submit: 'ok',
      dotyczy: 'aktualizacja',
      // dotyczy: 'nowy',
      siec,
      pasmo,
      carrier: '',
      RNC: '',
      lac,
      sektor_1,
      sektor_2,
      sektor_3,
      sektor_4: '',
      sektor_5: '',
      sektor_6: '',
      sektor_7: '',
      sektor_8: '',
      sektor_9: '',
      sektor_0: '',
      wojewodztwo,
      miejscowosc,
      lokalizacja,
      adres_email: 'PLEASE_PROVIDE_YOUR_EMAIL',
      rownanie: 'siedem'
    }

    requestBody.lokalizacja += ` \r\n\r\n eNodeB: ${eNodeB}`;
    
    console.log('');
    console.log('Dane do wysłania: ');
    console.log('');
    console.log(requestBody);
    console.log('');

    try {
      await pressAnyKey('Wcisnij dowolny przycisk aby wysłać dane, lub CTRL+C aby anulować', {
        ctrlC: "reject"
      })
    } catch (err) {
      console.log('Wysłanie danych anulowane !');
      return;
    }
    
    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    
    axios.post(btsSearchUrl, qs.stringify(requestBody), config)
      .then((result) => {

        console.log('');
        console.log('Dane wysłane poprawnie !');
        console.log('');

        // zapis do pliku
        const logData = {
          data: requestBody,
          url: btsSearchUrl,
          status: result.status,
          body: result.data
        };

        const timeStamp = moment().format('DD-MM-YYYY_HHmm');
        const fileName = `${logsDir}/${siec}_${eNodeB}_${pasmo}_${timeStamp}.json`;

        fs.appendFileSync(fileName, JSON.stringify(logData, null, 2));
        
        resolve(true);
      })
      .catch((err) => {
        console.error(err);
        reject(err);
      });
  });
}

const send = async () => {

  const btsDataArr = await readCsvInputData();
  
  for(let i=0; i<btsDataArr.length; i++) {
    await sendDataToBtsSearch(btsDataArr[i]);
  }
};

send();



// const requestBody = {
//   submit: 'ok',
//   dotyczy: 'aktualizacja',
//   siec: 'Orange',
//   pasmo: 'LTE1800',
//   carrier: '',
//   RNC: '',
//   lac: '59602',
//   sektor_1: '31',
//   sektor_2: '32',
//   sektor_3: '33',
//   sektor_4: '',
//   sektor_5: '',
//   sektor_6: '',
//   sektor_7: '',
//   sektor_8: '',
//   sektor_9: '',
//   sektor_0: '',
//   wojewodztwo: 'Podlaskie [10]',
//   miejscowosc: 'Białystok',
//   lokalizacja: 'ul. Mickiewicza 43\r\n\r\neNodeB 196007',
//   adres_email: 'mariuszsadowski1987@gmail.com',
//   rownanie: 'siedem'
// };